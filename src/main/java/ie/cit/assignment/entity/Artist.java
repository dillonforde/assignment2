package ie.cit.assignment.entity;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Artist {

	private int id;

	List<Movements> movements;
	List<Artwork> artwork;
	
	private Birth birth;
	
	@JsonProperty("fc")
	private String fullName;
	
	private int birthYear;
	
	public String toString() {
		String artistAsString = "Name: " + fullName + "\nBorn: " + birthYear + "\nBirth Place: " + birth + "\nMovements: " + movements+"\nArtwork:"+artwork;
		return artistAsString;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public List<Artwork> getArtwork() {
		return artwork;
	}

	public void setArtwork(List<Artwork> artwork) {
		this.artwork = artwork;
	}
	
	public List<Movements> getMovements() {
		return movements;
	}

	public void setMovements(List<Movements> movements) {
		this.movements = movements;
	}

	public Birth getBirth() {
		return birth;
	}

	public void setBirth(Birth birth) {
		this.birth = birth;
	}

	public int getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(int birthYear) {
		this.birthYear = birthYear;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
}
