package ie.cit.assignment.repository;

import java.util.List;

import javax.sql.DataSource;

import ie.cit.assignment.entity.Artist;
import ie.cit.assignment.entity.Movements;

public interface ArtistRepository {

	void setDataSource(DataSource dataSource);
	
	Movements findMovement(String movement);
	
	List<Artist> findAll();
	
	Artist get(String id);
	
	void save(Artist artist);
	
}
