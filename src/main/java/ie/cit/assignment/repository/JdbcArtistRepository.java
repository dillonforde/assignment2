package ie.cit.assignment.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import ie.cit.assignment.entity.Artist;
import ie.cit.assignment.entity.Artwork;
import ie.cit.assignment.entity.Movements;
import ie.cit.assignment.entity.Place;

@Repository
public class JdbcArtistRepository implements ArtistRepository{

	private JdbcTemplate jdbcTemplate;
	
	private SimpleJdbcInsert insertArtist;
	private SimpleJdbcInsert insertMovement;
	private SimpleJdbcInsert insertBirth;
	private SimpleJdbcInsert insertArtistMovements;
	
	@Autowired
	public JdbcArtistRepository(JdbcTemplate jdbcTemplate) {
		
		this.jdbcTemplate = jdbcTemplate;
		
	}
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		
		insertArtist = new SimpleJdbcInsert(dataSource)
				.withTableName("artists")
				//.usingColumns("fullName", "birthYear")
				.usingColumns("fullName", "birthYear", "id");
				//.usingGeneratedKeyColumns("id");
		
		insertMovement = new SimpleJdbcInsert(dataSource)
				.withTableName("movements")
				//.usingColumns("name")
				.usingColumns("name","id");
				//.usingGeneratedKeyColumns("id");
		
		insertBirth = new SimpleJdbcInsert(dataSource)
				.withTableName("births")
				.usingColumns("artistId", "place");
		
		insertArtistMovements = new SimpleJdbcInsert(dataSource)
				.withTableName("artist_movements")
				.usingColumns("artistId", "movementId");
		
	}
	
	public Movements findMovement(String movementName){
	
		try {
			
			String sql = "SELECT * FROM movements WHERE name = ?";
		 
			Movements movement = (Movements)jdbcTemplate.queryForObject(
					sql, new Object[] { movementName }, 
					new BeanPropertyRowMapper<Movements>(Movements.class));
			
			System.out.println(movement);
			
			return movement;
		
		} catch (EmptyResultDataAccessException e) {
			
			return null;
			
		}	
		
	}
	
	@Override
	public Artist get(String id) {
		
		try {
			
			String sql = "SELECT * FROM artists WHERE id = ?";
		 
			Artist artist = (Artist)jdbcTemplate.queryForObject(
					sql, new Object[] { id }, 
					new BeanPropertyRowMapper<Artist>(Artist.class));
			
			
			//* Grabbing artist movements 
			
			String sql1 = "SELECT movementId FROM artist_movements WHERE artistId ="+id;
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql1);
			
			List<Movements> movements = new ArrayList<Movements>();
			String sql2 = "SELECT * FROM movements WHERE id = ?";
			int movementId;
			
			for(Map row : rows){
				Movements movement = new Movements();
				movementId = (int)(row.get("movementId"));
				movement = (Movements)jdbcTemplate.queryForObject(
						sql2, new Object[] { movementId }, 
						new BeanPropertyRowMapper<Movements>(Movements.class));
				
				movements.add(movement);
			}
			
			 //Grabbing Artist's Artwork	
			String sql3 = "SELECT * FROM artworks WHERE artistId ="+id+" LIMIT 10";
			List<Map<String, Object>> rows2 = jdbcTemplate.queryForList(sql3);
			
			List<Artwork> artworks = new ArrayList<Artwork>();
			String artworkName;
			for(Map row2 : rows2){
				Artwork artwork = new Artwork();
				artwork.setTitle((String)(row2.get("title")));
				artwork.setUrl((String)(row2.get("url")));
				artworks.add(artwork);
			}
			
			
			
			/*
			String sql3 = "SELECT * FROM artworks WHERE artistId = ?";
			
			List<Artwork> artworks = new ArrayList<Artwork>();
			 
			 Artwork artwork = (Artwork)jdbcTemplate.queryForObject(
					sql3, new Object[] { id }, 
					new BeanPropertyRowMapper<Artwork>(Artwork.class));
			artworks.add(artwork);
			
			artist.setArtwork(artworks);*/
			artist.setArtwork(artworks);
			artist.setMovements(movements);
			System.out.println(artist);
			return artist;
		
		} catch (EmptyResultDataAccessException e) {
			
			return null;
			
		}	
		
	}
	
	public List<Artist> findAll() {
		
		String sql = "SELECT * FROM artists ORDER BY RAND() LIMIT 30";
		
		try {
			
			List<Artist> artists  = jdbcTemplate.query(sql,
					new BeanPropertyRowMapper<Artist>(Artist.class));
			
			return artists;
		
		} catch (EmptyResultDataAccessException e) {
			
			return null;
			
		}
		
	}
	
	public void save(Artist artist) {
		
		//This method is called in the Application class after the Artist has been read in from the JSON
		//Saves artist to Database
		try{
			Map<String, Object> artistParams = new HashMap<String, Object>();
			artistParams.put("fullName", artist.getFullName());
			artistParams.put("birthYear", artist.getBirthYear());
			artistParams.put("id", artist.getId());
			insertArtist.execute(artistParams);
			 int artistId = artist.getId();
			 
			
			
			//System.out.println("ARTISTID INT!!!: " + artist.getId());
			
			//System.out.println("ARTIST ADDED: " + artist.getFullName());
			
			Map<String, Object> birthParams = new HashMap<String, Object>();
			
			
			if (artist.getBirth() != null){
				if (artist.getBirth().getPlace() != null){
					birthParams.put("artistId", artistId);
					birthParams.put("place", artist.getBirth().getPlace());
					
					insertBirth.execute(birthParams);
					
					//System.out.println("BIRTH ADDED: " + artist.getBirth().getPlace());
				}
			}
			Map<String, Object> movementParams = new HashMap<String, Object>();
			Map<String, Object> artistMovementsParams = new HashMap<String, Object>();
			
			for(int i = 0; i < artist.getMovements().size(); i++) {
				
				String movementName = artist.getMovements().get(i).getName();
				int movementId = artist.getMovements().get(i).getId();
				Movements movement = findMovement(movementName);
				
				if(movement == null) {
					
					movementParams.put("name", movementName);
					movementParams.put("id", movementId);
					insertMovement.execute(movementParams);					
					//System.out.println("MOVEMENT ADDED: " + movementName);
					
					artistMovementsParams.clear();
				
				}
				artistMovementsParams.put("artistId", artistId);
				artistMovementsParams.put("movementId", movementId);
				insertArtistMovements.execute(artistMovementsParams);
				
			}
			
			
		}catch(Exception e){
			
		}
	}
	
}
