package ie.cit.assignment.repository;

import org.springframework.web.client.RestTemplate;

import ie.cit.assignment.entity.Results;

public class ResultGetter {
	
	public Results get(){
	    String url = "https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=5&q=tate+gallery&type=video&key=AIzaSyBFezeODpY2SZiktKpZNAPYhOOope4y_Jc";
	    RestTemplate template = new RestTemplate();
	    Results results = template.getForObject(url, Results.class);
	    return results;
	}
}
