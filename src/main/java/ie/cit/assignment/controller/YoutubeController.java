package ie.cit.assignment.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import ie.cit.assignment.entity.Results;
import ie.cit.assignment.repository.ResultGetter;

@Controller
@RequestMapping("/youtube")
public class YoutubeController {

	@RequestMapping("")
	public String main(Model model){
		
		ResultGetter getResults = new ResultGetter();
		Results results = getResults.get();
		model.addAttribute("results",results);
		return "/youtube/youtube";
	}
	

}
